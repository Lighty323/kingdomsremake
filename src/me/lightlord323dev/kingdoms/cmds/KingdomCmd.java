package me.lightlord323dev.kingdoms.cmds;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Rank;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimType;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Selection;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.SelectionHandler;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.memberhandlers.Invitation;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.memberhandlers.InvitationManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.relationshiphandlers.AllyManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.wars.WarManager;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Blank on 7/2/2017.
 */
public class KingdomCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player))
            return true;

        Player p = (Player) sender;
        Msg m = new Msg();
        KingdomManager km = KingdomManager.i();


        if (args.length == 3) {
            if (args[0].equals("status")) {
                if (Bukkit.getPlayer(args[1]) == null) {
                    return true;
                }
                Player target = Bukkit.getPlayer(args[1]);
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString()) && km.getKingdom(target) == null && !km.getKingdom(target).getName().equals(km.getKingdom(p).getName())) {
                    return true;
                }
                Kingdom k = km.getKingdom(p);
                switch (args[2]) {
                    case "knight":
                        k.setRank(target, Rank.KNIGHT);
                        break;
                    case "member":
                        k.setRank(target, Rank.MEMBER);
                        break;
                    case "jester":
                        k.setRank(target, Rank.JESTER);
                        break;
                    case "queen":
                        k.setRank(target, Rank.QUEEN);
                        break;
                }
            }
            return true;
        }

        if (args.length == 2) {
            if (args[0].equals("create")) {
                Location center = p.getLocation();
                Location loc1 = new Location(center.getWorld(), center.getBlockX() + 3, center.getBlockY(), center.getBlockZ() + 4);
                Location loc2 = new Location(center.getWorld(), center.getBlockX() - 4, center.getBlockY(), center.getBlockZ() - 3);
                Cuboid c = new Cuboid(loc1, loc2);

                if (ClaimManager.i().doesOverlap(c)) {
                    m.sendRed(p, "Part or all of this land belongs to another kingdom, please start your kingdom in an unclaimed area.");
                    return true;
                }

                KingdomManager.i().createKingdom(p, args[1]);
                if (KingdomManager.i().getKingdom(p) != null && KingdomManager.i().getKingdom(p).getName().equals(args[1]) && KingdomManager.i().getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString()) && ClaimManager.i().getClaim(KingdomManager.i().getKingdom(p)) == null) {
                    KingdomManager.i().getKingdom(p).claim(p.getLocation(), ClaimType.MAINCLAIM);
                }
            }

            if (args[0].equals("giveplot")) {
                if (Bukkit.getPlayer(args[1]) == null) {
                    return true;
                }
                if(km.getKingdom(Bukkit.getPlayer(args[1])) == null) {
                    return true;
                }
                if(!km.getKingdom(Bukkit.getPlayer(args[1])).getName().equals(km.getKingdom(p).getName())) {
                    m.sendRed(p, "That player does not belong to your kingdom!");
                    return true;
                }
                if (args[1].equals(p.getName())) {
                    return true;
                }
                if (km.getKingdom(p) != null && km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    Selection s = new Selection(p, Bukkit.getPlayer(args[1]));
                    SelectionHandler.i().addSelection(s);
                    m.sendGold(p, "Left click the 2 corners of the plot you want to give to " + args[1]);
                }
            }

            if (args[0].equals("invite")) {
                if (Bukkit.getPlayer(args[1]) == null) {
                    return true;
                }
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                Player target = Bukkit.getPlayer(args[1]);
                if (km.getKingdom(target) != null) {
                    m.sendRed(p, "This player already belongs to a kingdom.");
                    return true;
                }
                if(km.getKingdom(p).getBlacklist().contains(target.getUniqueId().toString())) {
                    m.sendRed(p, "This player cannot join your kingdom!");
                    return true;
                }
                InvitationManager.i().invitePlayer(target, km.getKingdom(p));
                km.getKingdom(p).announce(args[1] + " has been invited to the kingdom!");
                m.sendGold(target, "You have been invited to the " + km.getKingdom(p).getName() + " kingdom.\nDo </kingdoms accept> to accept the invitation.");
            }

            if (args[0].equals("kick")) {
                if (Bukkit.getPlayer(args[1]) == null) {
                    return true;
                }
                Player target = Bukkit.getPlayer(args[1]);
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString()) && km.getKingdom(target) == null && !km.getKingdom(target).getName().equals(km.getKingdom(p).getName())) {
                    return true;
                }
                km.removePlayer(km.getKingdom(p).getName(), target);
            }
            if (args[0].equals("allies")) {
                Kingdom k = km.getKingdom(args[1]);
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                k.listAllies(p);
            }

            if (args[0].equals("ally")) {
                Kingdom k = km.getKingdom(args[1]);
                Kingdom kp = km.getKingdom(p);
                if (kp == null && !kp.getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if(kp.getName().equals(args[1])) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                if (!k.canAlly()) {
                    m.sendRed(p, "That kingdom already has 3 allies.");
                    return true;
                }
                if (!kp.canAlly()) {
                    m.sendRed(p, "Your kingdom already has 3 allies.");
                    return true;
                }

                Kingdom k1 = km.getKingdom(p);
                if (k1.isAlly(k)) {
                    m.sendRed(p, "This kingdom is already an ally!");
                    return true;
                }
                if (!AllyManager.i().hasRequest(k1, k)) {
                    k1.announce("An alliance request has been sent to the kingdom of " + k.getName());
                    AllyManager.i().createRequest(k1, k);
                    return true;
                } else {
                    if (AllyManager.i().isIssuerOfRequest(k1, AllyManager.i().getRequest(k1, k))) {
                        m.sendRed(p, "You have already requested an alliance with this kingdom.");
                        return true;
                    } else {
                        AllyManager.i().acceptRequest(k1, k);
                    }
                }
            }

            if (args[0].equals("enemy")) {
                Kingdom k = km.getKingdom(args[1]);
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                Kingdom k1 = km.getKingdom(p);
                if(k1.getName().equals(args[1])) {
                    return true;
                }
                if (k.isEnemy(k1)) {
                    m.sendRed(p, "That kingdom is already an enemy!");
                    return true;
                }
                k1.announce("The kingdom of " + k.getName() + " is now an enemy!");
                k.announce("The kingdom of " + k1.getName() + " is now an enemy!");
                k1.addEnemy(k);
                k.addEnemy(k1);
            }

            if (args[0].equals("neutral")) {
                Kingdom k = km.getKingdom(args[1]);
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                Kingdom k1 = km.getKingdom(p);
                if (!k.isEnemy(k1) && !k.isAlly(k1)) {
                    m.sendRed(p, "That kingdom is already neutral!");
                    return true;
                }
                k.neutralizeKingdom(k1);
                k1.neutralizeKingdom(k);
                k1.announce("The kingdom of " + k.getName() + " is now neutral.");
                k.announce("The kingdom of " + k1.getName() + " is now neutral.");
            }

            if (args[0].equals("war")) {
                Kingdom k = km.getKingdom(args[1]);
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                if (Bukkit.getPlayer(k.getLeader()) == null) {
                    m.sendRed(p, "The king of that kingdom must be online for a war to be declared!");
                    return true;
                }
                if (WarManager.i().containsWar(k)) {
                    m.sendRed(p, "That kingdom is already in a war.");
                    return true;
                }
                Kingdom k1 = km.getKingdom(p);
                if(k1.isAlly(k)) {
                    m.sendRed(p, "That kingdom is an ally!");
                    return true;
                }
                k1.addEnemy(k1);
                k1.addEnemy(k);
                WarManager.i().createWar(k, k1);
            }

            if (args[0].equals("marry")) {
                Kingdom k = km.getKingdom(args[1]);
                Kingdom kp = km.getKingdom(p);
                if (kp == null && !kp.getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if(kp.getName().equals(args[1])) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                if (!k.getMarriage().equals("")) {
                    m.sendRed(p, "That kingdom already has a marriage declared.");
                    return true;
                }
                if (!kp.getMarriage().equals("")) {
                    m.sendRed(p, "Your kingdom already has a marriage declared.");
                    return true;
                }
                if(!kp.canAlly()) {
                    m.sendRed(p, "Your kingdom must have less than 3 allies!");
                    return true;
                }
                if(!k.canAlly()) {
                    m.sendRed(p, "That kingdom already has 3 allies!");
                    return true;
                }
                if (AllyManager.i().hasRequest(kp, k)) {
                    if (AllyManager.i().isIssuerOfRequest(kp, AllyManager.i().getRequest(kp, k))) {
                        m.sendRed(p, "You have already requested a marriage with the other kingdom's leader.");
                    } else {
                        AllyManager.i().removeRequest(kp, k);
                        kp.setMarriage(k.getName());
                        k.setMarriage(kp.getName());
                        k.addAlly(kp);
                        kp.addAlly(k);
                        k.getMembers().keySet().forEach(s -> {
                            if(k.getMembers().get(s) == Rank.QUEEN) {
                                k.setRank(Bukkit.getPlayer(UUID.fromString(s)), Rank.MEMBER);
                            }
                        });
                        kp.getMembers().keySet().forEach(s -> {
                            if(kp.getMembers().get(s) == Rank.QUEEN) {
                                kp.setRank(Bukkit.getPlayer(UUID.fromString(s)), Rank.MEMBER);
                            }
                        });
                        kp.announce("A marriage has been declared with the " + k.getName() + " kingdom!");
                        k.announce("A marriage has been declared with the " + kp.getName() + " kingdom!");
                    }
                } else {
                    AllyManager.i().addRequest(kp, k);
                    kp.announce("A marriage declaration has been requested with the kingdom of " + k.getName());
                    k.announce("A marriage declaration has been requested by the kingdom of " + kp.getName());
                }
            }

            if (args[0].equals("divorce")) {
                Kingdom k = km.getKingdom(args[1]);
                Kingdom kp = km.getKingdom(p);
                if (kp == null && !kp.getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                if (k.getMarriage().equals("")) {
                    m.sendRed(p, "That kingdom does not have a marriage declared.");
                    return true;
                }
                if (kp.getMarriage().equals("")) {
                    m.sendRed(p, "Your kingdom does not have a marriage declared.");
                    return true;
                }
                if(kp.getMarriage().equals(k.getName())) {
                    kp.setMarriage("");
                    k.setMarriage("");
                    k.neutralizeKingdom(kp);
                    kp.neutralizeKingdom(k);
                    Bukkit.broadcastMessage(ChatColor.GOLD+"The kingdom of "+kp.getName()+" has divorced the kingdom of "+k.getName());
                }
            }

            if (args[0].equals("invest")) {
                Kingdom k = km.getKingdom(p);
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                if (!isInteger(args[1])) {
                    m.sendRed(p, "Please specify a valid integer.");
                    return true;
                }
                int i = Integer.parseInt(args[1]);
                if(i > Main.i().econ.getBalance(p)) {
                    m.sendRed(p, "You do not have that much money!");
                    return true;
                }
                Main.i().econ.withdrawPlayer(p, i);
                k.addBank(i);
                k.announce(p.getName() + " has invested " + i + "$ into the kingdom!");
            }

            if (args[0].equals("withdraw")) {
                Kingdom k = km.getKingdom(p);
                if (!k.getLeader().toString().equals(p.getUniqueId().toString())) {
                    m.sendRed(p, "Only the king can withdraw from the kingdom!");
                    return true;
                }
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                if (!isInteger(args[1])) {
                    m.sendRed(p, "Please specify a valid integer.");
                    return true;
                }
                int i = Integer.parseInt(args[1]);
                if(k.getBank() < i) {
                    m.sendRed(p, "The bank doesn't contain that much money!");
                    return true;
                }
                k.withdrawFromBank(p, i);
                k.announce("The king has withdrawn " + i + "$ from the kingdom!");
            }

            if (args[0].equals("enemies")) {
                Kingdom k = km.getKingdom(args[1]);
                if (k == null) {
                    m.sendRed(p, "That kingdom does not exist.");
                    return true;
                }
                k.listEnemies(p);
            }
            return true;
        }

        if (args.length == 1) {
            if(args[0].equals("rogue")) {
                Kingdom k = km.getKingdom(p);
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                if(k.getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                for (String s : k.getMembers().keySet()) {
                    if(k.getMembers().get(s) == Rank.ROGUE) {
                        return true;
                    }
                }
                k.setRank(p, Rank.ROGUE);
                k.announce(p.getName()+" has gone rogue!");
            }
            if (args[0].equals("upgrade")) {
                Kingdom k = km.getKingdom(p);
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                if (!k.getLeader().toString().equals(p.getUniqueId().toString())) {
                    m.sendRed(p, "Only the king can upgrade the kingdom!");
                    return true;
                }
                int i = Main.i().cf.getConfig().getInt("cost-per-upgrade");
                if (k.getBank() >= i) {
                    k.upgrade();
                    k.reduceBank(i);
                    k.announce("The king has upgraded the kingdom!");
                    k.announce("The kingdom can now accommodate " + k.getSize() + " members!");
                } else {
                    m.sendRed(p, "Upgrading the kingdom requires " + i + "$ to be in the kingdom's bank.\nDo </kingdoms invest <amount>> to invest into the kingdom.");
                    return true;
                }
            }
            if (args[0].equals("bank")) {
                Kingdom k = km.getKingdom(p);
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                m.sendBlue(p, "Bank: "+ ChatColor.GOLD+k.getBank());
            }
            if (args[0].equals("list")) {
                km.listKingdoms(p);
            }
            if (args[0].equals("accept")) {
                if (InvitationManager.i().isInvited(p) == null) {
                    m.sendRed(p, "You have not been invited to any kingdom.");
                    return true;
                }
                if (km.getKingdom(p) != null) {
                    m.sendRed(p, "You already belong to a kingdom.");
                    return true;
                }
                Invitation i = InvitationManager.i().isInvited(p);
                km.addPlayer(i.getK().getName(), p);
                InvitationManager.i().unInvite(p);
            }
            if (args[0].equals("leave")) {
                Kingdom k = km.getKingdom(p);
                if (k != null) {
                    if (k.getLeader().toString().equals(p.getUniqueId().toString())) {
                        km.disbandKingdom(p);
                    } else {
                        km.removePlayer(k.getName(), p);
                    }
                }
            }
            if (args[0].equals("outpost")) {
                if (km.getKingdom(p) == null && !km.getKingdom(p).getLeader().toString().equals(p.getUniqueId().toString())) {
                    return true;
                }
                Kingdom k = km.getKingdom(p);
                if (ClaimManager.i().canHaveOutpost(k)) {
                    ClaimManager.i().addOutpost(p, k);
                    k.announce("The king has created an outpost!");
                } else {
                    m.sendRed(p, "You cannot yet establish an outpost!");
                }
            }
            if(args[0].equals("stats")) {
                Kingdom k = km.getKingdom(p);
                if (k == null) {
                    m.sendRed(p, "You do not belong to a kingdom!");
                    return true;
                }
                int i = 0;
                for(Cuboid c : ClaimManager.i().getClaim(k).getTerritories().keySet()) {
                    if(ClaimManager.i().getClaim(k).getTerritories().get(c) == ClaimType.OUTPOST) {
                        i++;
                    }
                }
                m.sendGold(p, "========== Stats ==========");
                p.sendMessage(ChatColor.BLUE+"Kingdom: "+ChatColor.GOLD+k.getName()+"\n"+ChatColor.BLUE+"Size: "+ChatColor.GOLD+k.getSize()+"\n"+ChatColor.BLUE+"Number of members: "+ChatColor.GOLD+k.getMembers().keySet().size()+"\n"+ChatColor.BLUE+"Number of outposts: "+ChatColor.GOLD+i);
                k.listAllies(p);
                k.listEnemies(p);
            }
            return true;
        }
        sendCommandList(p);
        return true;
    }

    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    public void sendCommandList(Player p) {
        Msg m = new Msg();
        m.sendGold(p, "========== Kingdom Arguments ==========");
        m.sendBlue(p, "/kingdoms create <kingdom>  - establishes a kingdom!");
        m.sendBlue(p, "/kingdoms invite <player>   - invites a player to your kingdom.");
        m.sendBlue(p, "/kingdoms accept            - accepts the latest invitation received from a kingdom.");
        m.sendBlue(p, "/kingdoms status <player> <queen/knight/jester/member> - sets player's status.");
        m.sendBlue(p, "/kingdoms rogue             - makes you go rogue against your kingdom.");
        m.sendBlue(p, "/kingdoms stats             - shows you the stats of your kingdom.");
        m.sendBlue(p, "/kingdoms bank              - shows total amount of money invested by players.");
        m.sendBlue(p, "/kingdoms invest <amount>   - invests the amount of money into the kingdom.");
        m.sendBlue(p, "/kingdoms withdraw <amount> - withdraws amount of money from the kingdom (king only).");
        m.sendBlue(p, "/kingdoms upgrade           - upgrades the size of the kingdom.");
        m.sendBlue(p, "/kingdoms kick <player>     - kicks a player from your kingdom.");
        m.sendBlue(p, "/kingdoms leave             - lets you leave your kingdom.\n ");
        m.sendGold(p, "===========  Land Arguments  ==========");
        m.sendBlue(p, "/kingdoms giveplot <player> - gives specified plot to the player.");
        m.sendBlue(p, "/kingdoms outpost           - establishes an outpost for your kingdom.\n");
        m.sendGold(p, "===========  Relationship Arguments ===========");
        m.sendBlue(p, "/kingdoms ally <kingdom>    - declares kingdom as an ally.");
        m.sendBlue(p, "/kingdoms enemy <kingdom>   - declares kingdom as an enemy.");
        m.sendBlue(p, "/kingdoms neutral <kingdom> - declares kingdom as neutral.");
        m.sendBlue(p, "/kingdoms marry <kingdom>   - declares marriage and allies the specified kingdom!");
        m.sendBlue(p, "/kingdoms divorce <kingdom> - revokes a declared marriage and neutralizes the specified kingdom!");
        m.sendBlue(p, "/kingdoms war <kingdom>     - declares war against the kingdom.\n");
        m.sendGold(p, "===========  General Arguments ===========");
        m.sendBlue(p, "/kingdoms list - returns a list of all kingdoms.");
        m.sendBlue(p, "/kingdoms allies <kingdom> - returns a list of all allies of the kingdom.");
        m.sendBlue(p, "/kingdoms enemies <kingdom> - returns a list of all enemies of the kingdom.");
    }

}
