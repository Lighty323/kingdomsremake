package me.lightlord323dev.kingdoms.handlers.kingdomhandlers;

/**
 * Created by Blank on 7/2/2017.
 */
public enum Rank {

    MEMBER("Member"), KNIGHT("Knight"), QUEEN("Queen"), JESTER("Jester"), ROGUE("Rogue");

    String name;

    Rank(String name) {
        this.name = name;
    }

}
