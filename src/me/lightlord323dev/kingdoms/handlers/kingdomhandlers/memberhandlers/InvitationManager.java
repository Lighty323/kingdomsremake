package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.memberhandlers;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Blank on 7/3/2017.
 */
public class InvitationManager {

    private List<Invitation> inv = new ArrayList<>();

    private static InvitationManager im;

    public static InvitationManager i() {
        if (im == null)
            im = new InvitationManager();
        return im;
    }

    public Invitation invitePlayer(Player p, Kingdom k) {
        Invitation invitation = new Invitation(p, k);
        if (isInvited(p) != null) {
            unInvite(p);
        }
        inv.add(invitation);
        return null;
    }

    public void unInvite(Player p) {
        Invitation invitation = null;
        for (Invitation i : inv) {
            if (p.getUniqueId().toString().equals(i.getP().getUniqueId().toString())) {
                invitation = i;
            }
        }
        inv.remove(invitation);
    }

    public Invitation isInvited(Player p) {
        for (Invitation i : inv) {
            if (p.getUniqueId().toString().equals(i.getP().getUniqueId().toString())) {
                return i;
            }
        }
        return null;
    }

}
