package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.memberhandlers;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import org.bukkit.entity.Player;

/**
 * Created by Blank on 7/3/2017.
 */
public class Invitation {

    private Player p;
    private Kingdom k;

    public Invitation(Player p, Kingdom k) {
        this.p = p;
        this.k = k;
    }

    public Player getP() {
        return p;
    }

    public Kingdom getK() {
        return k;
    }
}
