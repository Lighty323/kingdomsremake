package me.lightlord323dev.kingdoms.handlers.kingdomhandlers;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.files.KingdomData;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Claim;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimType;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Blank on 7/1/2017.
 */
public class KingdomManager {


    private static KingdomManager sm;

    public static KingdomManager i() {
        if (sm == null)
            sm = new KingdomManager();
        return sm;
    }

    KingdomData data = Main.i().kd;
    FileConfiguration c = data.getConfig();
    Msg m = new Msg();
    private List<Kingdom> kingdoms = new ArrayList<>();

    public void init() {
        for(String s : c.getKeys(false)) {
            Kingdom k = (Kingdom)c.get(s);
            kingdoms.add(k);
        }
    }

    public void disinit() {
        for(Kingdom k : kingdoms) {
            c.set(k.getName(), k);
        }
        data.save();
    }

    public void listKingdoms(Player p) {
        if(kingdoms.isEmpty()) {
            m.sendRed(p, "There are currently no kingdoms");
        } else {
            StringBuilder s = new StringBuilder();
            for (Kingdom kingdom : kingdoms) {
                s.append(kingdom.getName()+", ");
            }
            s.deleteCharAt(s.length() - 1);
            s.deleteCharAt(s.length() - 1);
            p.sendMessage(ChatColor.GOLD+"Kingdoms: \n"+ChatColor.BLUE+s.toString());
        }
    }

    public void createKingdom(Player leader, String name) {

        if (kingdomExists(name)) {
            m.sendRed(leader, "A kingdom with that name already exists.");
            return;
        }

        if (getKingdom(leader) != null) {
            m.sendRed(leader, "You are already in a kingdom.");
            return;
        }

        Kingdom s = new Kingdom(name, leader.getUniqueId());
        kingdoms.add(s);
        m.sendGold(leader, "You are now the king of the kingdom " + ChatColor.YELLOW + name + ChatColor.GOLD + ".");
        Bukkit.getServer().broadcastMessage(ChatColor.BLUE+leader.getName()+""+ChatColor.GOLD+" is now the king of the "+name+" kingdom!");
        return;
    }

    public void disbandKingdom(Player p) {
        Kingdom sq = null;
        for (Kingdom s : kingdoms) {
            if (s.getLeader().toString().equals(p.getUniqueId().toString())) {
                sq = s;
            }
        }
        if (sq == null) {
            return;
        }
        for (String s : sq.getMembers().keySet()) {
            Player pl = Bukkit.getPlayer(UUID.fromString(s));
            if(pl != null) {
                AttributeInstance a = pl.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                a.setBaseValue(20.0);
            }
        }
        kingdoms.remove(sq);
        ClaimManager.i().removeClaim(sq);
        c.set(sq.getName(), null);
        data.save();
        Bukkit.broadcastMessage(ChatColor.GOLD+""+ChatColor.BOLD+"The "+sq.getName()+" kingdom was disbanded!");
    }

    public void addPlayer(String squad, Player p) {
        if (kingdomExists(squad)) {
            Kingdom s = getKingdom(squad);
            if (!isFull(squad)) {
                s.getMembers().put(p.getUniqueId().toString(), Rank.MEMBER);
                ClaimManager.i().getClaim(s).expandMainClaim();
                s.announce(p.getName() + " has joined the kingdom!");
                s.announce("The kingdom's land has been expanded!");
                switch (s.getMembers().size()) {
                    case 9:
                        s.announce("Your kingdom can now setup a new outpost!");
                        break;
                    case 19:
                        s.announce("Your kingdom can now setup a new outpost!");
                        break;
                    case 29:
                        s.announce("Your kingdom can now setup a new outpost!");
                        break;
                    case 49:
                        s.announce("Your kingdom can now setup a new outpost!");
                        break;
                }
            } else {
                m.sendRed(p, "This kingdom has reached it's capacity!");
            }
        }
    }

    public void removePlayer(String squad, Player p) {
        if (kingdomExists(squad)) {
            Kingdom s = getKingdom(squad);
            if (!s.getLeader().toString().equals(p.getUniqueId().toString()) && getKingdom(p) != null && getKingdom(p).getName().equals(squad)) {
                s.getMembers().remove(p.getUniqueId().toString());
                ClaimManager.i().getClaim(s).removePlot(p);
                ClaimManager.i().getClaim(s).contractMainClaim();
                s.announce(p.getName() + " has left the kingdom!");
                s.announce("The kingdom has lost some land!");
                m.sendGold(p, "You have been removed from the kingdom.");
                AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                a.setBaseValue(20.0);
                if (!ClaimManager.i().getOutposts(s).isEmpty()) {
                    Claim c = ClaimManager.i().getClaim(s);
                    switch (ClaimManager.i().getOutposts(s).size()) {
                        case 1:
                            if (s.getMembers().size() < 9) {
                                c.removeTerritory(ClaimType.OUTPOST);
                            }
                            break;
                        case 2:
                            if (s.getMembers().size() < 19) {
                                c.removeTerritory(ClaimType.OUTPOST);
                            }
                            break;
                        case 3:
                            if (s.getMembers().size() < 29) {
                                c.removeTerritory(ClaimType.OUTPOST);
                            }
                            break;
                        case 4:
                            if (s.getMembers().size() < 49) {
                                c.removeTerritory(ClaimType.OUTPOST);
                            }
                            break;
                    }
                }
            }
        }
    }

    public void mergeKingdoms(Kingdom k, Kingdom toBeMerged) {
        k.setSize(toBeMerged.getSize() + k.getSize());
        for (String s : toBeMerged.getMembers().keySet()) {
            if(toBeMerged.getMembers().get(s) != Rank.MEMBER) {
                removePlayer(toBeMerged.getName(), Bukkit.getPlayer(UUID.fromString(s)));
            } else {
                addPlayer(k.getName(), Bukkit.getPlayer(UUID.fromString(s)));
            }
        }
        ClaimManager.i().mergeClaims(k, toBeMerged);
        kingdoms.remove(toBeMerged);
        toBeMerged = null;
    }

    public boolean isFull(String name) {
        if (getKingdom(name).getMembers().size() == 10) {
            return true;
        }
        return false;
    }

    public boolean kingdomExists(String name) {
        for (Kingdom s : kingdoms) {
            if (s.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public Kingdom getKingdom(String name) {
        for (Kingdom s : kingdoms) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        return null;
    }

    public Kingdom getKingdom(Player p) {
        for (Kingdom s : kingdoms) {
            if (s.getLeader().toString().equals(p.getUniqueId().toString()) || s.getMembers().keySet().contains(p.getUniqueId().toString())) {
                return s;
            }
        }
        return null;
    }

    public List<Kingdom> getKingdoms() {
        return kingdoms;
    }

    public void setSquads(List<Kingdom> kingdoms) {
        this.kingdoms = kingdoms;
    }


}
