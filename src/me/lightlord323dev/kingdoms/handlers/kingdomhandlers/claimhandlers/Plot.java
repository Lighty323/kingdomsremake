package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

import me.lightlord323dev.kingdoms.utils.Cuboid;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Blank on 7/2/2017.
 */
@SerializableAs("Plot")
public class Plot implements ConfigurationSerializable {

    private String kingdom;
    private UUID owner;
    private Cuboid plot;


    public Plot(String kingdom, UUID owner, Cuboid plot) {
        this.kingdom = kingdom;
        this.owner = owner;
        this.plot = plot;
    }

    public Plot(Map<String, Object> map) {
        kingdom = (String) map.get("kingdom");
        owner = UUID.fromString((String) map.get("owner"));
        plot = (Cuboid) map.get("plot");
    }

    public boolean contains(Location l) {
        if (plot.contains(l)) {
            return true;
        }
        return false;
    }

    public String getKingdom() {
        return kingdom;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public Cuboid getPlot() {
        return plot;
    }

    public void setPlot(Cuboid plot) {
        this.plot = plot;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("kingdom", kingdom);
        map.put("owner", owner.toString());
        map.put("plot", plot);
        return map;
    }

    public static Plot deserialize(Map<String, Object> map) {
        String kingdom = (String) map.get("kingdom");
        UUID owner = UUID.fromString((String) map.get("owner"));
        Cuboid plot = (Cuboid) map.get("plot");
        return new Plot(kingdom, owner, plot);
    }
}
