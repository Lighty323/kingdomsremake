package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Blank on 7/3/2017.
 */
public class Selection {

    private Player owner;
    private Player target;
    private List<Location> l;

    public Selection(Player owner, Player target) {
        this.owner = owner;
        this.target = target;
        l = new ArrayList<>();
    }

    public Player getOwner() {
        return owner;
    }

    public Player getTarget() {
        return target;
    }

    public List<Location> getL() {
        return l;
    }
}
