package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Rank;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Blank on 7/2/2017.
 */
@SerializableAs("Claim")
public class Claim implements ConfigurationSerializable {

    private Kingdom k;
    private HashMap<Cuboid, ClaimType> territories;
    private List<Plot> plots;

    public Claim(Cuboid claim, Kingdom k, ClaimType type) {
        this.k = k;
        territories = new HashMap<>();
        territories.put(claim, type);
        plots = new ArrayList<>();
    }

    public Claim(Map<String, Object> map) {
        k = (Kingdom) map.get("kingdom");
        territories = (HashMap<Cuboid, ClaimType>) map.get("territories");
        plots = (List<Plot>) map.get("plots");
    }

    public Claim(Cuboid claim, ClaimType type, List<Plot> plots) {
        territories = new HashMap<>();
        territories.put(claim, type);
        this.plots = plots;
    }

    public Claim(Kingdom k, HashMap<Cuboid, ClaimType> territories, List<Plot> plots) {
        this.k = k;
        this.territories = territories;
        this.plots = plots;
    }

    public void expandMainClaim() {
        for (Cuboid cube : territories.keySet()) {
            if (territories.get(cube) == ClaimType.MAINCLAIM) {
                territories.remove(cube);
                int amt = Main.i().cf.getConfig().getInt("blocks-to-expand");
                cube = cube.expand(Cuboid.CuboidDirection.North, amt);
                cube = cube.expand(Cuboid.CuboidDirection.South, amt);
                cube = cube.expand(Cuboid.CuboidDirection.East, amt);
                cube = cube.expand(Cuboid.CuboidDirection.West, amt);
                territories.put(cube, ClaimType.MAINCLAIM);
            }
        }
    }

    public void expandMainClaim(int amt) {
        for (Cuboid cube : territories.keySet()) {
            if (territories.get(cube) == ClaimType.MAINCLAIM) {
                territories.remove(cube);
                cube = cube.expand(Cuboid.CuboidDirection.North, amt);
                cube = cube.expand(Cuboid.CuboidDirection.South, amt);
                cube = cube.expand(Cuboid.CuboidDirection.East, amt);
                cube = cube.expand(Cuboid.CuboidDirection.West, amt);
                territories.put(cube, ClaimType.MAINCLAIM);
            }
        }
    }

    public void removePlot(Player p) {
        Plot pl = null;
        for (Plot plot : plots) {
            if(plot.getOwner().toString().equals(p.getUniqueId().toString())) {
                pl = plot;
            }
        }
        if(pl != null) {
            plots.remove(pl);
        }
    }

    public void removeTerritory(ClaimType type) {
        for (Cuboid c : territories.keySet()) {
            if (territories.get(c) == ClaimType.OUTPOST) {
                territories.remove(c);
            }
        }
    }

    public void contractMainClaim() {
        for (Cuboid cube : territories.keySet()) {
            if (territories.get(cube) == ClaimType.MAINCLAIM) {
                territories.remove(cube);
                int amt = Main.i().cf.getConfig().getInt("blocks-to-expand") * (-1);
                cube = cube.expand(Cuboid.CuboidDirection.North, amt);
                cube = cube.expand(Cuboid.CuboidDirection.South, amt);
                cube = cube.expand(Cuboid.CuboidDirection.East, amt);
                cube = cube.expand(Cuboid.CuboidDirection.West, amt);
                territories.put(cube, ClaimType.MAINCLAIM);
            }
        }
    }

    public boolean contains(Location l) {
        for (Cuboid c : territories.keySet()) {
            if (c.contains(l)) {
                return true;
            }
        }
        for (Plot p : plots) {
            if (p.contains(l)) {
                return true;
            }
        }
        return false;
    }

    public void addPlot(Plot p) {
        plots.add(p);
    }

    public boolean isPartOfPlot(Location l) {
        for (Plot p : plots) {
            if (p.contains(l)) {
                return true;
            }
        }
        return false;
    }

    public Plot getPlot(Location l) {
        for (Plot p : plots) {
            if (p.contains(l)) {
                return p;
            }
        }
        return null;
    }

    public void addTerritory(Cuboid cuboid, ClaimType claimType) {
        territories.put(cuboid, claimType);
    }

    public boolean canBuild(Player p, Location l) {
        Kingdom kd = KingdomManager.i().getKingdom(p);
        if(kd == null) {
            return false;
        }
        if (k.getName().equals(kd.getName())) {
            if (kd.getLeader().toString().equals(p.getUniqueId().toString())) {
                if(isPartOfPlot(l) && !getPlot(l).getOwner().toString().equals(p.getUniqueId().toString())) {
                    return false;
                }
                return true;
            }
            if(kd.getMembers().get(p.getUniqueId().toString()) == Rank.QUEEN) {
                if(isPartOfPlot(l) && !getPlot(l).getOwner().toString().equals(p.getUniqueId().toString())) {
                    return false;
                }
                return true;
            }
            if (isPartOfPlot(l) && getPlot(l).getOwner().toString().equals(p.getUniqueId().toString())) {
                return true;
            }
        }
        return false;
    }

    public Kingdom getK() {
        return k;
    }

    public void setK(Kingdom k) {
        this.k = k;
    }

    public List<Plot> getPlots() {
        return plots;
    }

    public void setPlots(List<Plot> plots) {
        this.plots = plots;
    }

    public HashMap<Cuboid, ClaimType> getTerritories() {
        return territories;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("kingdom", k);
        HashMap<Cuboid, String> s = new HashMap<>();
        for(Cuboid c : territories.keySet()) {
            s.put(c, territories.get(c).toString());
        }
        map.put("territories", s);
        map.put("plots", plots);
        return map;
    }

    public static Claim deserialize(Map<String, Object> map) {
        Kingdom k = (Kingdom) map.get("kingdom");
        HashMap<Cuboid, String> s = (HashMap<Cuboid, String>) map.get("territories");
        HashMap<Cuboid, ClaimType> territories = new HashMap<>();
        for (Cuboid cuboid : s.keySet()) {
            territories.put(cuboid, ClaimType.valueOf(s.get(cuboid)));
        }
        List<Plot> plots = (List<Plot>) map.get("plots");
        return new Claim(k, territories, plots);
    }

}
