package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

/**
 * Created by Blank on 7/2/2017.
 */
public enum ClaimType {


    MAINCLAIM("MainClaim"), OUTPOST("Outpost"), MERGED_KINGDOM("MergedKingdom");
    String name;
    ClaimType(String name) {
        this.name = name;
    }

}
