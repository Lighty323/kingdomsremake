package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Blank on 7/3/2017.
 */
public class SelectionHandler {

    private static SelectionHandler sh;

    public static SelectionHandler i() {
        if (sh == null)
            sh = new SelectionHandler();
        return sh;
    }

    List<Selection> selectionList = new ArrayList<>();

    public void addSelection(Selection s) {
        selectionList.add(s);
    }

    public Selection getSelection(Player p) {
        for (Selection sel : selectionList) {
            if (sel.getOwner().getUniqueId().toString().equals(p.getUniqueId().toString())) {
                return sel;
            }
        }
        return null;
    }

    public boolean containsSelection(Player p) {
        for (Selection sel : selectionList) {
            if (sel.getOwner().getUniqueId().toString().equals(p.getUniqueId().toString())) {
                return true;
            }
        }
        return false;
    }

    public void removeSelection(Player p) {
        Selection s = null;
        for (Selection sel : selectionList) {
            if (sel.getOwner().getUniqueId().toString().equals(p.getUniqueId().toString())) {
                s = sel;
            }
        }
        if (s == null) {
            return;
        }
        selectionList.remove(s);
    }

}
