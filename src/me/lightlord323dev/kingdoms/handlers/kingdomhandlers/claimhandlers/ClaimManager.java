package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Blank on 7/2/2017.
 */
public class ClaimManager {

    private static ClaimManager cm;

    public static ClaimManager i() {
        if (cm == null)
            cm = new ClaimManager();
        return cm;
    }

    private List<Claim> claims = new ArrayList<>();
    public HashMap<String, HashMap<String, List<Location>>> selections = new HashMap<>();
    FileConfiguration c = Main.i().ld.getConfig();

    public void init() {
        for (String s : c.getKeys(false)) {
            Claim c = (Claim)this.c.get(s);
            claims.add(c);
        }
    }

    public void disinit() {
        for (Claim claim : claims) {
            c.set(claim.getK().getName(), claim);
        }
        Main.i().ld.save();
    }

    public Claim createClaim(Kingdom k, Cuboid cube, ClaimType type) {
        Claim c = new Claim(cube, k, type);
        claims.add(c);
        return c;
    }

    public void removeClaim(Kingdom k) {
        Claim c = null;
        for (Claim claim : claims) {
            if(claim.getK().getName().equals(k.getName())) {
                c = claim;
            }
        }
        if(c != null) {
            claims.remove(c);
        }
        this.c.set(k.getName(), null);
        Main.i().ld.save();
    }

    public void addOutpost(Player p, Kingdom k) {
        k.claim(p.getLocation(), ClaimType.OUTPOST);
    }

    public boolean canHaveOutpost(Kingdom k) {
        if (k.getMembers().size() >= 9) {
            if (getOutposts(k).size() <= 0)
                return true;
        }
        if (k.getMembers().size() >= 19) {
            if (getOutposts(k).size() <= 1)
                return true;
        }
        if (k.getMembers().size() >= 29) {
            if (getOutposts(k).size() <= 2)
                return true;
        }
        if (k.getMembers().size() >= 49) {
            if (getOutposts(k).size() <= 3)
                return true;
        }

        return false;
    }

    public List<Cuboid> getOutposts(Kingdom k) {
        Claim claim = getClaim(k);
        List<Cuboid> outposts = new ArrayList<>();
        for (Cuboid c : getClaim(k).getTerritories().keySet()) {
            if (claim.getTerritories().get(c) == ClaimType.OUTPOST) {
                outposts.add(c);
            }
        }
        return outposts;
    }

    public void mergeClaims(Kingdom k, Kingdom toBeMerged) {
        Claim c = getClaim(k);
        Claim c1 = getClaim(toBeMerged);
        for (Cuboid cuboid : c1.getTerritories().keySet()) {
            c.addTerritory(cuboid, ClaimType.MERGED_KINGDOM);
        }
        for (Plot plot : c1.getPlots()) {
            plot.setKingdom(k.getName());
            c1.addPlot(plot);
        }
        claims.remove(c1);
    }

    public Kingdom isClaimed(Location l) {
        for (Claim c : claims) {
            if (c.contains(l)) {
                return c.getK();
            }
        }
        return null;
    }

    public boolean doesOverlap(Cuboid c) {
        for (Block b : c) {
            Location l = b.getLocation();
            if (isClaimed(l) != null) {
                return true;
            }
        }
        return false;
    }


    public boolean isWilderness(Location l) {
        for (Claim c : claims) {
            if (c.contains(l)) {
                return false;
            }
        }
        return true;
    }

    public Claim getClaim(Kingdom k) {
        for (Claim c : claims) {
            if (c.getK().getName().equals(k.getName())) {
                return c;
            }
        }
        return null;
    }

    public void addPlot(Kingdom k, Plot p) {
        Claim c = getClaim(k);
        c.addPlot(p);
    }

}
