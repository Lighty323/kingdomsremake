package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.relationshiphandlers;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Blank on 7/6/2017.
 */
public class AllyManager {

    private static AllyManager am;
    public static AllyManager i() {
        if(am == null)
            am = new AllyManager();
        return am;
    }

    private List<AllyRequest> requests = new ArrayList<>();

    public void addRequest(Kingdom k1, Kingdom k2) {
        AllyRequest r = new AllyRequest(k1, k2);
        requests.add(r);
    }

    public void createRequest(Kingdom k1, Kingdom k2) {
        AllyRequest r = new AllyRequest(k1, k2);
        requests.add(r);
        k2.announce("The kingdom of "+k1.getName()+" has requested to be your ally.\n"+"The king must type /kingdoms ally "+k1.getName()+" to accept their request.");
    }

    public boolean isIssuerOfRequest(Kingdom k, AllyRequest r) {
        if(r.getK1().getName().equals(k.getName())) {
            return true;
        }
        return false;
    }

    public AllyRequest getRequest(Kingdom k1, Kingdom k2) {
        for (AllyRequest request : requests) {
            if((k1.getName().equals(request.getK1().getName()) || k1.getName().equals(request.getK2().getName())) && (k2.getName().equals(request.getK1().getName()) || k2.getName().equals(request.getK2().getName()))) {
                return request;
            }
        }
        return null;
    }

    public boolean hasRequest(Kingdom k1, Kingdom k2) {
        for (AllyRequest request : requests) {
            if((k1.getName().equals(request.getK1().getName()) || k1.getName().equals(request.getK2().getName())) && (k2.getName().equals(request.getK1().getName()) || k2.getName().equals(request.getK2().getName()))) {
                return true;
            }
        }
        return false;
    }

    public void acceptRequest(Kingdom k1, Kingdom k2) {
        AllyRequest r = null;
        for (AllyRequest request : requests) {
            if((k1.getName().equals(request.getK1().getName()) || k1.getName().equals(request.getK2().getName())) && (k2.getName().equals(request.getK1().getName()) || k2.getName().equals(request.getK2().getName()))) {
                r = request;
                k1.addAlly(k2);
                k2.addAlly(k1);
                k1.announce("Your kingdom is now an ally of the "+k2.getName()+" kingdom!");
                k2.announce("Your kingdom is now an ally of the "+k1.getName()+" kingdom!");
            }
        }
        if(r != null) {
            requests.remove(r);
        }
    }

    public void removeRequest(Kingdom k1, Kingdom k2) {
        requests.remove(getRequest(k1, k2));
    }

}
