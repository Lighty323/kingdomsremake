package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.relationshiphandlers;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;

/**
 * Created by Blank on 7/6/2017.
 */
public class AllyRequest {

    private Kingdom k1;
    private Kingdom k2;

    public AllyRequest(Kingdom k1, Kingdom k2) {
        this.k1 = k1;
        this.k2 = k2;
    }

    public Kingdom getK1() {
        return k1;
    }

    public Kingdom getK2() {
        return k2;
    }
}
