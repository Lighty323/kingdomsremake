package me.lightlord323dev.kingdoms.handlers.kingdomhandlers;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimType;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Blank on 7/1/2017.
 */
@SerializableAs("Kingdom")
public class Kingdom implements ConfigurationSerializable {

    Msg m = new Msg();

    private String name;
    private UUID leader;
    private HashMap<String, Rank> members = new HashMap<>();
    private int size;
    private int bank;
    private String marriage;
    private List<String> allies = new ArrayList<>();
    private List<String> enemies = new ArrayList<>();
    private List<String> blacklist = new ArrayList<>();

    public Kingdom(String name, UUID leader) {
        this.name = name;
        this.leader = leader;
        this.size = 10;
        this.bank = 0;
        this.marriage = "";
    }

    public Kingdom(Map<String, Object> map) {
        this.name = (String) map.get("name");
        this.leader = UUID.fromString((String) map.get("leader"));
        HashMap<String, String> s = (HashMap<String, String>) map.get("members");
        this.members = new HashMap<>();
        for (String s1 : s.keySet()) {
            this.members.put(s1, Rank.valueOf(s.get(s1)));
        }
        this.size = (int) map.get("size");
        this.bank = (int) map.get("bank");
        this.marriage = (String) map.get("marriage");
        this.setAllies((List<String>) map.get("allies"));
        this.setEnemies((List<String>) map.get("enemies"));
        this.setBlacklist((List<String>) map.get("blacklist"));
    }

    public Kingdom(String name, UUID leader, HashMap<String, Rank> members, int size, int bank, String marriage) {
        this.name = name;
        this.leader = leader;
        this.members = members;
        this.size = size;
        this.bank = bank;
        this.marriage = marriage;
    }

    public int getBank() {
        return bank;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public UUID getLeader() {
        return leader;
    }

    public HashMap<String, Rank> getMembers() {
        return members;
    }

    public void setBlacklist(List<String> blacklist) {
        this.blacklist = blacklist;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<String> getBlacklist() {
        return blacklist;
    }

    public void blacklist(String s) {
        if(!blacklist.contains(s))
            blacklist.add(s);
    }

    public void announce(String msg) {
        if (Bukkit.getPlayer(leader) != null) {
            m.sendGold(Bukkit.getPlayer(leader), msg);
        }
        for (String s : members.keySet()) {
            if (Bukkit.getPlayer(UUID.fromString(s)) != null) {
                m.sendGold(Bukkit.getPlayer(UUID.fromString(s)), msg);
            }
        }
    }

    public void withdrawFromBank(Player p, int amt) {
        reduceBank(amt);
        Main.i().econ.depositPlayer(p, amt);
    }

    public int addBank(int amt) {
        this.bank += amt;
        return this.bank;
    }

    public int reduceBank(int amt) {
        this.bank -= amt;
        return this.bank;
    }

    public void claim(Location center, ClaimType type) {
        Location loc1 = new Location(center.getWorld(), center.getBlockX() + 3, center.getWorld().getMaxHeight(), center.getBlockZ() + 4);
        Location loc2 = new Location(center.getWorld(), center.getBlockX() - 4, 0, center.getBlockZ() - 3);
        Cuboid c = new Cuboid(loc1, loc2);
        if (type == ClaimType.MAINCLAIM) {
            ClaimManager.i().createClaim(this, c, ClaimType.MAINCLAIM);
        } else {
            ClaimManager.i().createClaim(this, c, ClaimType.OUTPOST);
        }
    }

    public boolean canAlly() {
        if (allies.size() < 3)
            return true;
        return false;
    }

    public void setRank(Player p, Rank rank) {
        if (!members.keySet().contains(p.getUniqueId().toString()))
            return;
        if (members.get(p.getUniqueId().toString()) == rank)
            return;
        if (rank == Rank.QUEEN) {
            if (!marriage.equals("")) {
                m.sendRed(Bukkit.getPlayer(leader), "You are already married!");
                return;
            }
            if (members.values().contains(Rank.QUEEN)) {
                m.sendRed(Bukkit.getPlayer(leader), "There can be only 1 Queen.");
                return;
            } else {
                AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                a.setBaseValue(26.0);
            }
        }
        Msg m = new Msg();
        if (rank == Rank.JESTER) {
            long i = members.values().stream().filter((r) -> r == Rank.JESTER).count();
            if (i == 2) {
                m.sendRed(Bukkit.getPlayer(leader), "There can be no more Jesters.");
                return;
            } else {
                AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                a.setBaseValue(16.0);
            }
        }
        if (rank == Rank.KNIGHT) {
            AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            a.setBaseValue(24.0);
        }
        if (rank == Rank.MEMBER) {
            AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            a.setBaseValue(20.0);
        }
        if (rank == Rank.ROGUE) {
            AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            a.setBaseValue(20.0);
        }
        members.put(p.getUniqueId().toString(), rank);
        if(rank != Rank.ROGUE) {
            announce(p.getName() + "'s status has been set to " + rank.toString() + "!");
        }
    }

    public void upgrade() {
        size = size + Main.i().cf.getConfig().getInt("amount-per-upgrade");
    }

    public boolean isAlly(Kingdom k) {
        if (allies.contains(k.getName())) {
            return true;
        }
        return false;
    }

    public boolean isEnemy(Kingdom k) {
        if (enemies.contains(k.getName())) {
            return true;
        }
        return false;
    }

    public void addEnemy(Kingdom k) {
        if (!enemies.contains(k.getName())) {
            if (allies.contains(k.getName())) {
                allies.remove(k.getName());
            }
            enemies.add(k.getName());
        }
    }

    public void addAlly(Kingdom k) {
        if (allies.size() < 3 && !allies.contains(k.getName())) {
            if (enemies.contains(k.getName())) {
                enemies.remove(k.getName());
            }
            allies.add(k.getName());
        }
    }

    public void listAllies(Player p) {
        StringBuilder s = new StringBuilder();
        allies.removeIf(u -> KingdomManager.i().getKingdom(u) == null);
        for (String a : allies) {
            s.append(a + ", ");
        }
        p.sendMessage(ChatColor.GOLD + name + "'s Allies:");
        p.sendMessage(ChatColor.LIGHT_PURPLE + s.toString());
    }

    public void neutralizeKingdom(Kingdom k) {
        if (isEnemy(k)) {
            enemies.remove(k.getName());
        }
        if (isAlly(k)) {
            allies.remove(k.getName());
        }
    }

    public void listEnemies(Player p) {
        StringBuilder s = new StringBuilder();
        enemies.removeIf(u -> KingdomManager.i().getKingdom(u) == null);
        for (String a : enemies) {

            s.append(a + ", ");

        }
        p.sendMessage(ChatColor.GOLD + name + "'s Enemies:");
        p.sendMessage(ChatColor.RED + s.toString());
    }

    public void removeFromMembers(Player p) {
        members.remove(p.getUniqueId().toString());
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setAllies(List<String> allies) {
        this.allies = allies;
    }

    public void setEnemies(List<String> enemies) {
        this.enemies = enemies;
    }

    public void setLeader(UUID leader) {
        this.leader = leader;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("leader", leader.toString());
        HashMap<String, String> s = new HashMap<>();
        for (String c : members.keySet()) {
            s.put(c, members.get(c).toString());
        }
        map.put("members", s);
        map.put("size", size);
        map.put("allies", allies);
        map.put("enemies", enemies);
        map.put("bank", bank);
        map.put("marriage", marriage);
        map.put("blacklist", blacklist);
        return map;
    }

    public static Kingdom deserialize(Map<String, Object> map) {
        String name = (String) map.get("name");
        UUID leader = UUID.fromString((String) map.get("leader"));
        HashMap<String, String> s = (HashMap<String, String>) map.get("members");
        HashMap<String, Rank> members = new HashMap<>();
        for (String s1 : s.keySet()) {
            members.put(s1, Rank.valueOf(s.get(s1)));
        }
        int size = (int) map.get("size");
        int bank = (int) map.get("bank");
        String marriage = (String) map.get("marriage");
        Kingdom k = new Kingdom(name, leader, members, size, bank, marriage);
        k.setAllies((List<String>) map.get("allies"));
        k.setEnemies((List<String>) map.get("enemies"));
        k.setBlacklist((List<String>) map.get("blacklist"));
        return k;
    }

}
