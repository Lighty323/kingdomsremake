package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.wars;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import org.bukkit.Bukkit;

/**
 * Created by Blank on 7/6/2017.
 */
public class War {

    private Kingdom k1;
    private Kingdom k2;
    private int timeframe = Main.i().cf.getConfig().getInt("time-frame-for-war-in-minutes") * 60;
    private int i = 10;
    private int s = 0;
    private boolean started = false;

    public War(Kingdom k1, Kingdom k2) {
        this.k1 = k1;
        this.k2 = k2;
    }

    public void startWar() {
        started = true;
    }

    public Kingdom getK1() {
        return k1;
    }

    public Kingdom getK2() {
        return k2;
    }

    public int getTimeframe() {
        return timeframe;
    }

    public boolean isStarted() {
        return started;
    }
}
