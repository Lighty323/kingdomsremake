package me.lightlord323dev.kingdoms.handlers.kingdomhandlers.wars;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Blank on 7/6/2017.
 */
public class WarManager {

    private static WarManager wm;
    public static WarManager i() {
        if(wm == null)
            wm = new WarManager();
        return wm;
    }

    private List<War> wars = new ArrayList<>();
    private ConcurrentHashMap<War, Integer> warPool = new ConcurrentHashMap<>();
    private boolean poolStarted = false;
    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);

    public void createWar(Kingdom k1, Kingdom k2) {
        Bukkit.getServer().broadcastMessage(ChatColor.GRAY+"=========================================");
        Bukkit.getServer().broadcastMessage(ChatColor.BLUE+"A war has been declared between the two kingdoms of "+k1.getName()+" and "+k2.getName()+"!");
        Bukkit.getServer().broadcastMessage(ChatColor.GRAY+"=========================================");
        if(k1.isAlly(k2)){
            k1.addEnemy(k2);
        }
        War w = new War(k1, k2);
        startWarCountdown(w);
        wars.add(w);
    }

    public boolean isInWar(Kingdom k) {
        for (War war : wars) {
            if(war.getK1().getName().equals(k.getName()) || war.getK2().getName().equals(k.getName())) {
                if (war.isStarted()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsWar(Kingdom k) {
        for (War war : wars) {
            if(war.getK1().getName().equals(k.getName()) || war.getK2().getName().equals(k.getName())) {
                    return true;
            }
        }
        return false;
    }

    public boolean isInWar(Kingdom k1, Kingdom k2) {
        for (War war : wars) {
            if((war.getK1().getName().equals(k1.getName()) || war.getK1().getName().equals(k2.getName())) && (war.getK2().getName().equals(k2.getName()) || war.getK2().getName().equals(k1.getName()))) {
                if (war.isStarted()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void cancelWar(Kingdom k1, Kingdom k2) {
        War w = null;
        for (War war : wars) {
            if((war.getK1().getName().equals(k1.getName()) || war.getK1().getName().equals(k2.getName())) && (war.getK2().getName().equals(k2.getName()) || war.getK2().getName().equals(k1.getName()))) {
                w = war;
            }
        }
        if(w != null) {
            wars.remove(w);
        }
    }

    public void startWarCountdown(War war) {
        Kingdom k1 = war.getK1();
        Kingdom k2 = war.getK2();
        k1.announce("The war with the " + k2.getName() + " kingdom will start in " + (war.getTimeframe() / (60*60))+" hours.");
        k2.announce("The war with the " + k1.getName() + " kingdom will start in " + (war.getTimeframe() / (60*60))+" hours.");
        warPool.put(war, war.getTimeframe());
        startPool();
    }

    private void startPool() {
        if(poolStarted)
            return;
        if (warPool.isEmpty())
            return;
        this.executorService.scheduleAtFixedRate(() -> {
            warPool.entrySet().removeIf((e) -> e.getValue() == 0);
            warPool.replaceAll((w, i) -> i-1);
            warPool.keySet().forEach(w -> {
                int i = warPool.get(w);
                if(i == 60) {
                    w.getK1().announce(i + " seconds left until the war begins!");
                    w.getK2().announce(i + " seconds left until the war begins!");
                }
                if(i <= 10) {
                    if(i == 0) {
                        w.startWar();
                        w.getK1().announce("The war has begun!");
                        w.getK2().announce("The war has begun!");
                    } else {
                        w.getK1().announce(i + " seconds left until the war begins!");
                        w.getK2().announce(i + " seconds left until the war begins!");
                    }
                }
            });
        }, 1, 1, TimeUnit.SECONDS);
    }

}
