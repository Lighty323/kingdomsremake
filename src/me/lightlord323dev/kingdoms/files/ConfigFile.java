package me.lightlord323dev.kingdoms.files;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.utils.AbstractFile;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Blank on 7/3/2017.
 */
public class ConfigFile extends AbstractFile {

    public ConfigFile(Main main) {
        super(main, "config.yml");
    }

    public FileConfiguration getConfig() {
        return c;
    }

}
