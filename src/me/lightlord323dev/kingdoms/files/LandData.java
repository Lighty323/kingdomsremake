package me.lightlord323dev.kingdoms.files;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.utils.AbstractFile;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Blank on 7/3/2017.
 */
public class LandData extends AbstractFile {

    public LandData(Main main) {
        super(main, "landdata.yml");
    }

    public FileConfiguration getConfig() {
        return c;
    }

}
