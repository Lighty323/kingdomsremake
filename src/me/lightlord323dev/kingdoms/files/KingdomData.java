package me.lightlord323dev.kingdoms.files;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.utils.AbstractFile;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Blank on 7/1/2017.
 */
public class KingdomData extends AbstractFile {

    public KingdomData(Main main) {
        super(main, "kingdomdata.yml");
    }

    public FileConfiguration getConfig() {
        return c;
    }

}
