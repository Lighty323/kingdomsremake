package me.lightlord323dev.kingdoms;

import me.lightlord323dev.kingdoms.cmds.KingdomCmd;
import me.lightlord323dev.kingdoms.files.ConfigFile;
import me.lightlord323dev.kingdoms.files.KingdomData;
import me.lightlord323dev.kingdoms.files.LandData;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Claim;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Plot;
import me.lightlord323dev.kingdoms.listeners.*;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Blank on 7/1/2017.
 */
public class Main extends JavaPlugin {

    public static Economy econ = null;

    private static Main m;
    public static Main i() {
        return m;
    }

    public KingdomData kd;
    public ConfigFile cf;
    public LandData ld;

    static {
        ConfigurationSerialization.registerClass(Claim.class, "Claim");
        ConfigurationSerialization.registerClass(Kingdom.class, "Kingdom");
        ConfigurationSerialization.registerClass(Cuboid.class, "Cuboid");
        ConfigurationSerialization.registerClass(Plot.class, "Plot");
    }

    @Override
    public void onEnable() {
        m = this;
        if(!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        kd = new KingdomData(this);
        cf = new ConfigFile(this);
        ld = new LandData(this);
        if(cf.getConfig().getKeys(false).size() == 0) {
            cf.getConfig().set("blocks-to-expand", 5);
            cf.getConfig().set("cost-per-upgrade", 100);
            cf.getConfig().set("amount-per-upgrade", 5);
            cf.getConfig().set("expansion-on-win-war", "default");
            cf.getConfig().set("time-frame-for-war-in-minutes", 60);
            cf.save();
        }

        if (!setupEconomy() ) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        KingdomManager.i().init();
        ClaimManager.i().init();

        getCommand("kingdoms").setExecutor(new KingdomCmd());

        getServer().getPluginManager().registerEvents(new BlockBreak(), this);
        getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
        getServer().getPluginManager().registerEvents(new PlayerMove(), this);
        getServer().getPluginManager().registerEvents(new BlockPlace(), this);
        getServer().getPluginManager().registerEvents(new EntityDamage(), this);
        getServer().getPluginManager().registerEvents(new PlayerDeath(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getServer().getPluginManager().registerEvents(new CmdPrePrcs(), this);
    }

    @Override
    public void onDisable() {
        ClaimManager.i().disinit();
        KingdomManager.i().disinit();
        m = null;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

}
