package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Rank;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Blank on 7/6/2017.
 */
public class EntityDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if ((e.getDamager() instanceof Player) && (e.getEntity() instanceof Player)) {
            Player d = (Player) e.getDamager();
            Player p = (Player) e.getEntity();
            KingdomManager km = KingdomManager.i();
            Kingdom kd = km.getKingdom(d);
            Kingdom kp = km.getKingdom(p);
            Msg m = new Msg();
            if (kd == null || kp == null)
                return;
            if (kd.getName().equals(kp.getName())) {
                if(kd.getMembers().get(d.getUniqueId().toString()) == Rank.ROGUE || kp.getMembers().get(p.getUniqueId().toString()) == Rank.ROGUE) {

                } else {
                    m.sendRed(d, "You cannot damage a player from the same kingdom!");
                    e.setCancelled(true);
                }
                return;
            }
            if (kd.isAlly(kp)) {
                m.sendRed(d, "You cannot damage a player from an allied kingdom!");
                e.setCancelled(true);
                return;
            }
            Kingdom k = ClaimManager.i().isClaimed(p.getLocation());
            Kingdom k1 = ClaimManager.i().isClaimed(d.getLocation());
            if (k != null && k.getName().equals(kp.getName()) && k.getMembers().get(p.getUniqueId().toString()) == Rank.KNIGHT) {
                e.setDamage(e.getDamage() - 1);
                m.sendBlue(d, "This player has +1 defence!");
            }
            if (k1 != null && k1.getName().equals(kd.getName())) {
                if (k.getMembers().get(p.getUniqueId().toString()) == Rank.KNIGHT) {
                    e.setDamage(e.getDamage() + 1);
                    m.sendBlue(p, "This player has +1 attack!");
                }
                if (k.getMembers().get(p.getUniqueId().toString()) == Rank.JESTER) {
                    e.setDamage(e.getDamage() + 2);
                    m.sendBlue(p, "This player has +2 attack!");
                }
            }
        }
    }

}
