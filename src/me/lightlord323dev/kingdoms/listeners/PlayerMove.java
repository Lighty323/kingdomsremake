package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Plot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Blank on 7/5/2017.
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY() && e.getFrom().getBlockZ() == e.getTo().getBlockZ()) {
            return;
        }
        Kingdom kf = ClaimManager.i().isClaimed(e.getFrom());
        Kingdom kt = ClaimManager.i().isClaimed(e.getTo());
        if (kf == null && kt != null) {
            p.sendMessage(ChatColor.GOLD + "~~~ This land belongs to the " + ChatColor.YELLOW + kt.getName() + ChatColor.GOLD + " kingdom.");
            return;
        }
        if (kf != null && kt == null) {
            p.sendMessage(ChatColor.GOLD + "~~~ This land is a no mans land.");
            return;
        }
        if (kf != null && kt != null && !kf.getName().equals(kt.getName())) {
            p.sendMessage(ChatColor.GOLD + "~~~ This land belongs to the " + ChatColor.YELLOW + kt.getName() + ChatColor.GOLD + " kingdom.");
            return;
        }
        if(kf == null || kt == null) {
            return;
        }
        if (kf.getName().equals(kt.getName())) {
            Plot pf = ClaimManager.i().getClaim(kf).getPlot(e.getFrom());
            Plot pt = ClaimManager.i().getClaim(kt).getPlot(e.getTo());
            if (pf != null && pt != null) {
                if (!pf.getOwner().toString().equals(pt.getOwner().toString())) {
                    p.sendMessage(ChatColor.GOLD + "~~~ Land of " + kt.getName());
                    p.sendMessage(ChatColor.GOLD + "~~~ " + Bukkit.getOfflinePlayer(pt.getOwner()).getName() + "'s plot.");
                }
            }
            if (pf != null && pt == null) {
                p.sendMessage(ChatColor.GOLD + "~~~ Land of " + kt.getName());
                p.sendMessage(ChatColor.GOLD + "~~~ " + "Un-owned land.");
            }
            if (pt != null && pf == null) {

                p.sendMessage(ChatColor.GOLD + "~~~ Land of " + kt.getName());
                p.sendMessage(ChatColor.GOLD + "~~~ " + Bukkit.getOfflinePlayer(pt.getOwner()).getName() + "'s plot.");

            }
        }

    }

}
