package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Claim;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Blank on 7/2/2017.
 */
public class BlockBreak implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        Location loc = e.getBlock().getLocation();
        KingdomManager km = KingdomManager.i();
        ClaimManager cm = ClaimManager.i();
        Kingdom k = cm.isClaimed(loc);
        Msg m = new Msg();
        if (k != null) {
            Claim c = cm.getClaim(k);
            if(!c.canBuild(p, loc)) {
                e.setCancelled(true);
                Kingdom kingdom = km.getKingdom(p);
                if(kingdom != null && c.getK().getName().equals(kingdom.getName())) {
                    m.sendRed(p, "You do not have the privilege to build here.");
                } else {
                    m.sendRed(p, "This land belongs to the "+k.getName()+" kingdom!");
                }
            }
        }
    }

}
