package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.Main;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Rank;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.wars.WarManager;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Blank on 7/6/2017.
 */
public class PlayerDeath implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        KingdomManager km = KingdomManager.i();
        Kingdom k = km.getKingdom(p);
        if (p.getKiller() instanceof Player) {
            Player killer = p.getKiller();
            Kingdom kk = km.getKingdom(killer);
            if (kk != null && k != null) {
                WarManager wm = WarManager.i();
                if (k.getMembers().get(p.getUniqueId().toString()) == Rank.ROGUE && kk.getName().equals(k.getName())) {
                    km.removePlayer(k.getName(), p);
                    k.announce("The rogue " + p.getName() + " has fallen!");
                }
                if (k.getLeader().toString().equals(p.getUniqueId().toString())) {
                    if (kk.getName().equals(k.getName()) && kk.getMembers().get(killer.getUniqueId().toString()) == Rank.ROGUE) {
                        k.removeFromMembers(killer);
                        k.setLeader(killer.getUniqueId());
                        ClaimManager.i().getClaim(k).removePlot(p);
                        ClaimManager.i().getClaim(k).contractMainClaim();
                        k.announce(p.getName() + " has left the kingdom!");
                        k.announce("The kingdom has lost some land!");
                        new Msg().sendGold(p, "You have been removed from the kingdom.");
                        Bukkit.broadcastMessage(ChatColor.BLUE + killer.getName() + ChatColor.GOLD + " is now the new king of the " + k.getName() + " kingdom!");
                    }
                }
                if (wm.isInWar(k, kk)) {
                    if (k.getLeader().toString().equals(p.getUniqueId().toString())) {
                        wm.cancelWar(k, kk);
                        k.blacklist(k.getLeader().toString());
                        for (String s : k.getMembers().keySet()) {
                            k.blacklist(s);
                        }
                        km.disbandKingdom(p);
                        ClaimManager.i().removeClaim(k);
                        Bukkit.broadcastMessage(ChatColor.GOLD + "The king of the " + k.getName() + " kingdom has fallen!");
                        if (isInteger(Main.i().cf.getConfig().getString("expansion-on-war-win"))) {
                            int i = Integer.parseInt(Main.i().cf.getConfig().getString("expansion-on-war-win"));
                            ClaimManager.i().getClaim(kk).expandMainClaim(i);
                            kk.announce("The kingdom's land has been expanded!");
                        } else {
                            ClaimManager.i().getClaim(kk).expandMainClaim((int) k.getSize() / 10);
                            kk.announce("The kingdom's land has been expanded!");
                        }
                    }
                }
            }
        }
    }

    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

}
