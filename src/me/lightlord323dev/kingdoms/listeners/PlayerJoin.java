package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.wars.WarManager;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Blank on 7/6/2017.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        KingdomManager km = KingdomManager.i();
        Kingdom k = km.getKingdom(p);
        if(k == null) {
            AttributeInstance a = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            a.setBaseValue(20.0);
        }
        if(k != null && WarManager.i().isInWar(k)) {
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BASEDRUM, 2, 1);
            new Msg().sendRed(p, "Your kingdom is currently in war!");
        }
    }

}
