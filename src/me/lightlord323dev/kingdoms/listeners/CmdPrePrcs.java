package me.lightlord323dev.kingdoms.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Blank on 7/8/2017.
 */
public class CmdPrePrcs implements Listener {

    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e) {
        if(e.getMessage().startsWith("/pl") || e.getMessage().startsWith("/?")) {
            if(!e.getPlayer().isOp()) {
                e.setCancelled(true);
            }
        }
    }

}
