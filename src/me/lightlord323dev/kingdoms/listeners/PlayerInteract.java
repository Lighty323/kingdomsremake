package me.lightlord323dev.kingdoms.listeners;

import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.Kingdom;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.KingdomManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Claim;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.ClaimManager;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.Plot;
import me.lightlord323dev.kingdoms.handlers.kingdomhandlers.claimhandlers.SelectionHandler;
import me.lightlord323dev.kingdoms.utils.Cuboid;
import me.lightlord323dev.kingdoms.utils.Msg;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Blank on 7/2/2017.
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void onRight(PlayerInteractEvent e) {

        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            ClaimManager cm = ClaimManager.i();
            Player p = e.getPlayer();
            if (SelectionHandler.i().containsSelection(p)) {
                if (SelectionHandler.i().getSelection(p).getL().size() < 1) {
                    p.sendMessage(ChatColor.GOLD+"Position acknowledged. Left click the second position to complete selection.");
                    SelectionHandler.i().getSelection(p).getL().add(e.getClickedBlock().getLocation());
                } else {
                    p.sendMessage(ChatColor.GOLD+"Position acknowledged. A selection has been established.");
                    SelectionHandler.i().getSelection(p).getL().add(e.getClickedBlock().getLocation());
                    Location l1 = SelectionHandler.i().getSelection(p).getL().get(0);
                    Location l2 = SelectionHandler.i().getSelection(p).getL().get(1);
                    l1.setY(l1.getWorld().getMaxHeight());
                    l2.setY(0);
                    Cuboid c = new Cuboid(l1, l2);
                    cm.getClaim(KingdomManager.i().getKingdom(p)).addPlot(new Plot(KingdomManager.i().getKingdom(p).getName(), SelectionHandler.i().getSelection(p).getTarget().getUniqueId(), c));
                    p.sendMessage(ChatColor.GOLD+"The plot has been made.");
                    SelectionHandler.i().removeSelection(p);
                }
            }
        }

        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block b = e.getClickedBlock();
            Player p = e.getPlayer();
            if(b.getType() == Material.CHEST) {
                Location loc = b.getLocation();
                KingdomManager km = KingdomManager.i();
                ClaimManager cm = ClaimManager.i();
                Kingdom k = cm.isClaimed(loc);
                Msg m = new Msg();
                if (k != null) {
                    Claim c = cm.getClaim(k);
                    if(!c.canBuild(p, loc)) {
                        e.setCancelled(true);
                        Kingdom kingdom = km.getKingdom(p);
                        if(kingdom != null && c.getK().getName().equals(kingdom.getName())) {
                            m.sendRed(p, "You do not have the privilege to build here.");
                        } else {
                            m.sendRed(p, "This land belongs to the "+k.getName()+" kingdom!");
                        }
                    }
                }
            }
        }

    }

}
