package me.lightlord323dev.kingdoms.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Created by Blank on 7/1/2017.
 */
public class Msg {

    String prefix = ChatColor.RED + "[" + ChatColor.WHITE + "Kingdoms" + ChatColor.RED + "] ";

    public void sendRed(CommandSender s, String msg) {
        s.sendMessage(prefix + msg);
    }

    public void sendGreen(CommandSender s, String msg) {
        s.sendMessage(prefix + ChatColor.GREEN + msg);
    }

    public void sendGold(CommandSender s, String msg) {
        s.sendMessage(prefix + ChatColor.GOLD + msg);
    }

    public void sendBlue(CommandSender s, String msg) {
        s.sendMessage(prefix + ChatColor.BLUE + msg);
    }

    public void broadcastMsg(String msg) {
        Bukkit.getServer().broadcastMessage(prefix + ChatColor.GOLD + "" + ChatColor.BOLD + msg);
    }

}
