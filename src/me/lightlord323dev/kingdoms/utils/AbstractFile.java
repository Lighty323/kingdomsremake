package me.lightlord323dev.kingdoms.utils;

import me.lightlord323dev.kingdoms.Main;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Blank on 7/1/2017.
 */
public class AbstractFile {

    protected Main main;
    private File f;
    protected FileConfiguration c;

    public AbstractFile(Main main, String name) {
        this.main = main;
        this.f = new File(main.getDataFolder(), name);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.c = YamlConfiguration.loadConfiguration(f);
    }

    public void save() {
        try {
            c.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
